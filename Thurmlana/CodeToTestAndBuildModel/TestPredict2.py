from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2
import os
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
plt.rcParams["axes.grid"] = False

import keras
from keras.preprocessing import image
from keras.applications.mobilenet_v2 import preprocess_input
from keras.utils import to_categorical
from keras.applications.mobilenet_v2 import MobileNetV2
from keras.models import Sequential
from keras.layers import *
from keras.preprocessing import image
from keras.applications.mobilenet_v2 import preprocess_input

class2text = {
    0: 'Bacon',
    1: 'CrabStick',
}

data_dir = 'data'
num_classes = 2

# โหลด dataset

def load_dataset(data_df):
    '''Function to load dataset.'''
    x = []
    y = []
    for i, r in data_df.iterrows():
        f = os.path.join(data_dir, r['filename'])   # filename
        c = r['class']                              # class

        # Load and resize the image
        img = image.load_img(f, target_size=(224, 224))

        # Convert the Image object into a numpy array
        img = image.img_to_array(img)

        # Add to a list of images and classes
        x.append(img)
        y.append(c)
    x = np.asarray(x)
    y = np.asarray(y)
    return x, y

#โหลด train test

# Training set
train_df = pd.read_csv(os.path.join(data_dir, 'train_dataset.csv'))
raw_train_x, raw_train_y = load_dataset(train_df)

# Test set
test_df = pd.read_csv(os.path.join(data_dir, 'test_dataset.csv'))
raw_test_x, raw_test_y = load_dataset(test_df)

#ปรับแต่งข้อมูลเพื่อนำไปใช้

# Preprocess the input array
train_x = preprocess_input(raw_train_x)
test_x = preprocess_input(raw_test_x)

# Convert class data to one-hot format
train_y = to_categorical(raw_train_y, num_classes)
test_y = to_categorical(raw_test_y, num_classes)

# Convert the preprocessing images into `float32`
train_x = train_x.astype(np.float32)
test_x = test_x.astype(np.float32)

# Convert the one-hot vector into `int`
train_y = train_y.astype(np.int)
test_y = test_y.astype(np.int)

#ใช้ Model ของ MobileNetV2 แต่ไม่มี layer สุดท้ายที่เป็น NN ธรรมดา
base_model = MobileNetV2(
    input_shape=(224, 224, 3),
    weights='imagenet',  #ถ้าเอา comment นี้ออก คือการใช้ weight ของ model ที่ให้มา
    #weights=None,      #ถ้าเอา comment นี้ออก คือการเทรนเอง ไม่ได้ใช้ weight ของ model ที่ให้มา
    include_top=False)

#สร้าง layer สุดท้าย เพื่อแบ่งประเภทรูปภาพ

model = Sequential()

# Start with the base_model
model.add(base_model)

# Add a global spatial average pooling layer
model.add(GlobalAveragePooling2D())

# A fully-connected layer
model.add(Dense(1024, activation='relu'))

# A softmax layer -- note that we have 5 classes
model.add(Dense(num_classes, activation='softmax'))

# train layer สุดท้าย(อันใหม่)ที่สร้างขึ้นมา

# Freeze all layers in the MobileNetV2, except the ones we have just added.
for layer in base_model.layers:
    layer.trainable = False  #ปรับ Gradient เฉพาะ Layer ใหม่ อันอื่นคงเดิม

#กำหนดค่าต่างๆ

epochs = 40
batch_size = 16
learning_rate = 0.01

# SGD optimizer (you can use others)
optimizer = keras.optimizers.SGD(lr=learning_rate)

# Cross-entropy loss
loss = keras.losses.categorical_crossentropy

# Compile the model
model.compile(
    loss=loss,
    optimizer=optimizer,
    metrics=['accuracy'])

# ป้อนข้อมูลเพื่อ train model

hist = model.fit(
    train_x, train_y,
    batch_size=batch_size,
    epochs=epochs,
    verbose=1)

# ประเมินจากการ test

score = model.evaluate(x=test_x, y=test_y, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

#---------------------------------

#----Save Model-----------
#model.save('my_model.h5')

cap = cv2.VideoCapture("Mix.mp4") #rtsp://192.168.1.127:8080/h264_pcm.sdp

while True:
    
    ref,frame = cap.read()

    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv=cv2.GaussianBlur(hsv,(15,15),0)

    l_b = np.array([0, 0, 125])
    u_b = np.array([26, 255, 255]) #ตัวแรกที่เคยลอง 23 26 30

    mask = cv2.inRange(hsv, l_b, u_b)

    res = cv2.bitwise_and(frame, frame, mask=mask) #สีปกติที่ตัดฉากหลังออก

    #เสริม
    #thresh=cv2.adaptiveThreshold(mask,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,5,1)
    #kernel=np.ones((3,3),np.uint8)
    #mask=cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel,iterations=5)

    #เสริม
    contours,hierachy=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        area = cv2.contourArea(cnt)

        if area<30000 or area>120000: # Test ใช้ area<50000 or area>105000 Bacoon2 area<100000 or area>125000 Bacoon3 area<50000 or area>120000 Bacoon5 area<30000 or area>120000
            continue #ทดลองปูอัดที่ตัดแล้ว area<12500 or area>120000
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect).astype(int)
        #box = np.int0(box)
        cv2.drawContours(frame,[box],0,(0,255,0),2)
        #--------ตัดภาพ---------#
        width = int(rect[1][0])
        height = int(rect[1][1])

        src_pts = box.astype("float32")
        dst_pts = np.array([[0, height-1],
                        [0, 0],
                        [width-1, 0],
                        [width-1, height-1]], dtype="float32")
        M = cv2.getPerspectiveTransform(src_pts, dst_pts)
        warped = cv2.warpPerspective(frame, M, (width, height))
        images = []
        # Load and resize the image
        # ส่วนนี้อาจจะไม่ได้ใช้
        #img = image.load_img(warped, target_size=(224, 224))
        warped = cv2.resize(warped,(224,224))

        # Convert the Image object into a numpy array
        img = image.img_to_array(warped)

        # Add to a list of images
        images.append(img)

        # Visualize the resize image
        # print(f'[image {f}]')
        #plt.imshow(image.img_to_array(img).astype('uint8'))
        #plt.show()
        #plt.close("all")
        images = np.asarray(images)

        # Preprocess the input array
        x = preprocess_input(images)

        #print(f"Image shape: {x.shape}")

        #ทำนายภาพ
        probs = model.predict(x)
        temp=0.0
        E = probs[0][0]
        pre = 0
        for c in range(num_classes):
                #print(f'{class2text[c]} ({probs[0][c]*100:.2f}%)')
                temp=probs[0][c]
                if(temp>E):
                    E = temp
                    pre = c
        #print(f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)')
        cv2.putText(frame,f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)',(int(box[0][0]), int(box[1][1])),cv2.FONT_HERSHEY_SIMPLEX,0.8,(0,255,0),2)
        
    cv2.imshow("Show",frame)

    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()

