import cv2
import numpy as np

cap = cv2.VideoCapture("pork2.mp4") #rtsp://192.168.1.127:8080/h264_pcm.sdp

while True:

    ref,frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv=cv2.GaussianBlur(hsv,(15,15),0)

    l_b = np.array([0, 0, 125])
    u_b = np.array([35, 255, 255]) #ตัวแรกที่เคยลอง 23 26 30

    mask = cv2.inRange(hsv, l_b, u_b)

    res = cv2.bitwise_and(frame, frame, mask=mask) #สีปกติที่ตัดฉากหลังออก

    #เสริม
    #thresh=cv2.adaptiveThreshold(mask,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,5,1)
    kernel=np.ones((5,5),np.uint8)
    mask=cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel,iterations=4)

    cv2.imshow("mask", mask)
    count = 0
    #เสริม
    contours,hierachy=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        area = cv2.contourArea(cnt)

        if area<550000: #or area>6900000: # Test ใช้ area<50000 or area>105000 Bacoon2 area<100000 or area>125000 Bacoon3 area<50000 or area>120000 Bacoon5 area<30000 or area>120000
            continue #ทดลองปูอัดที่ตัดแล้ว area<12500 or area>120000 ปูอัด 5 ใช้ area<20000
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        cv2.drawContours(frame,[box],0,(0,255,0),2)
        count+=1
    
    cv2.putText(frame,str(count),(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
    cv2.imshow("Show",frame)

    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()
