from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from IPython.display import display

import os
import pandas as pd
import cv2
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams["axes.grid"] = False

import keras
from keras.preprocessing import image
from keras.applications.mobilenet_v2 import preprocess_input
#from keras.utils import to_categorical
from keras.applications.mobilenet_v2 import MobileNetV2
from keras.models import Sequential
from keras.layers import *
from keras.preprocessing import image
from keras.applications.mobilenet_v2 import preprocess_input
from keras.preprocessing.image import img_to_array

from flask import Flask, render_template, request, Response

app = Flask(__name__)

class2text = {
    0: 'Bacon',
    1: 'CrabStick',
    }

num_classes = 2

#------- Load Model-----------------
model = keras.models.load_model('my_model.h5')
#-----------------------------------


@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

def gen():
    textfile = open('number.txt')
    textfile2 = open('change.txt')
    number = int(textfile.readline())
    a = int(textfile2.readline())
    textfile.close()
    """Video streaming generator function."""
    cap = cv2.VideoCapture('Bacoon5.mp4')
    if a == 0 : #นับชิ้น
        # Read until video is completed
        while True:
            
            ref,frame = cap.read()

            hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            hsv=cv2.GaussianBlur(hsv,(15,15),0)

            l_b = np.array([0, 0, 125])
            u_b = np.array([26, 255, 255]) #ตัวแรกที่เคยลอง 23 26 30 #Pre ที่ทดลองได้ l_b [0,0,0] u_b [72,255,255] ที่เคยทำปกติ l_b [0,0,125] u_b [26,255,255]

            mask = cv2.inRange(hsv, l_b, u_b)

            res = cv2.bitwise_and(frame, frame, mask=mask) #สีปกติที่ตัดฉากหลังออก

            count_Bacon = 0
            count_CrabStick = 0
            #เสริม
            contours,hierachy=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            for cnt in contours:
                area = cv2.contourArea(cnt)

                if area<30000 or area>120000: # Test ใช้ area<50000 or area>105000 Bacoon2 area<100000 or area>125000 Bacoon3 area<50000 or area>120000 Bacoon5 area<30000 or area>120000
                    continue #ทดลองปูอัดที่ตัดแล้ว area<12500 or area>120000
                rect = cv2.minAreaRect(cnt)
                box = cv2.boxPoints(rect).astype(int)
                box = np.int0(box)
                cv2.drawContours(frame,[box],0,(0,255,0),2)
                #--------ตัดภาพ---------#
                width = int(rect[1][0])
                height = int(rect[1][1])

                src_pts = box.astype("float32")
                dst_pts = np.array([[0, height-1],
                                [0, 0],
                                [width-1, 0],
                                [width-1, height-1]], dtype="float32")
                M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                warped = cv2.warpPerspective(frame, M, (width, height))
                images = []
                
                warped = cv2.resize(warped,(224,224))

                # Convert the Image object into a numpy array
                img = img_to_array(warped)

                # Add to a list of images
                images.append(img)

                
                images = np.asarray(images)

                # Preprocess the input array
                x = preprocess_input(images)

                #print(f"Image shape: {x.shape}")

                #ทำนายภาพ
                probs = model.predict(x)
                temp=0.0
                E = probs[0][0]
                pre = 0
                for c in range(num_classes):
                        #print(f'{class2text[c]} ({probs[0][c]*100:.2f}%)')
                        temp=probs[0][c]
                        if(temp>E):
                            E = temp
                            pre = c
                #print(f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)')
                #นับจำนวนสิ่งที่ทำนาย
                if(class2text[pre]=="Bacon"):
                    count_Bacon+=1
                elif(class2text[pre]=="CrabStick"):
                    count_CrabStick+=1
                cv2.putText(frame,f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)',(int(box[0][0]), int(box[1][1])),cv2.FONT_HERSHEY_SIMPLEX,0.8,(0,255,0),2)
            #ถ้าจำนวนอะไรมากกว่าให้โชว์ว่าในถาดนั้นเป็นวัตถุดิบอะไร
            if(count_Bacon>count_CrabStick):
                cv2.putText(frame,"Bacon",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
                if(count_Bacon<number):
                    cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)
            elif(count_Bacon<count_CrabStick):
                cv2.putText(frame,"CrabStick",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
                if(count_CrabStick<number):
                    cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)
            else:
                cv2.putText(frame,"?",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
            frame = cv2.imencode('.jpg', frame)[1].tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
            key = cv2.waitKey(1)
            if key == 27:
                break
    elif a == 1 : #นับเป็นกลุ่มก้อน
           # Read until video is completed
            while True:
                
                ref,frame = cap.read()

                hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
                hsv=cv2.GaussianBlur(hsv,(15,15),0)

                l_b = np.array([0, 0, 125])
                u_b = np.array([35, 255, 255])

                mask = cv2.inRange(hsv, l_b, u_b)

                res = cv2.bitwise_and(frame, frame, mask=mask) #สีปกติที่ตัดฉากหลังออก
                count = 0
                #เสริม
                kernel=np.ones((5,5),np.uint8)
                mask=cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel,iterations=4)

                #เสริม
                contours,hierachy=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                for cnt in contours:
                    area = cv2.contourArea(cnt)
                    if area<550000: #or area>6900000: # Test ใช้ area<50000 or area>105000 Bacoon2 area<100000 or area>125000 Bacoon3 area<50000 or area>120000 Bacoon5 area<30000 or area>120000
                        continue
                    rect = cv2.minAreaRect(cnt)
                    box = cv2.boxPoints(rect)
                    box = np.int0(box)
                    count+=1
                if(count<1):
                    cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)

                frame = cv2.imencode('.jpg', frame)[1].tobytes()
                yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
                key = cv2.waitKey(1)
                if key == 27:
                    break

def gen2():

    textfile = open('number.txt')
    number = int(textfile.readline())
    textfile.close()
    """Video streaming generator function."""
    cap = cv2.VideoCapture('CrabSticks2.mp4')

    # Read until video is completed
    while True:
        
        ref,frame = cap.read()

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv=cv2.GaussianBlur(hsv,(15,15),0)

        l_b = np.array([0, 0, 125])
        u_b = np.array([26, 255, 255]) #ตัวแรกที่เคยลอง 23 26 30 #Pre ที่ทดลองได้ l_b [0,0,0] u_b [72,255,255] ที่เคยทำปกติ l_b [0,0,125] u_b [26,255,255]

        mask = cv2.inRange(hsv, l_b, u_b)

        res = cv2.bitwise_and(frame, frame, mask=mask) #สีปกติที่ตัดฉากหลังออก

        count_Bacon = 0
        count_CrabStick = 0
        #เสริม
        contours,hierachy=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contours:
            area = cv2.contourArea(cnt)

            if area<30000 or area>120000: # Test ใช้ area<50000 or area>105000 Bacoon2 area<100000 or area>125000 Bacoon3 area<50000 or area>120000 Bacoon5 area<30000 or area>120000
                continue #ทดลองปูอัดที่ตัดแล้ว area<12500 or area>120000
            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect).astype(int)
            box = np.int0(box)
            cv2.drawContours(frame,[box],0,(0,255,0),2)
            #--------ตัดภาพ---------#
            width = int(rect[1][0])
            height = int(rect[1][1])

            src_pts = box.astype("float32")
            dst_pts = np.array([[0, height-1],
                            [0, 0],
                            [width-1, 0],
                            [width-1, height-1]], dtype="float32")
            M = cv2.getPerspectiveTransform(src_pts, dst_pts)
            warped = cv2.warpPerspective(frame, M, (width, height))
            images = []

            warped = cv2.resize(warped,(224,224))

            # Convert the Image object into a numpy array
            img = img_to_array(warped)

            # Add to a list of images
            images.append(img)

            images = np.asarray(images)

            # Preprocess the input array
            x = preprocess_input(images)

            #print(f"Image shape: {x.shape}")

            #ทำนายภาพ
            probs = model.predict(x)
            temp=0.0
            new_probs = probs[0][0]
            pre = 0
            for c in range(num_classes):
                    #print(f'{class2text[c]} ({probs[0][c]*100:.2f}%)')
                    temp=probs[0][c]
                    if(temp>new_probs):
                        new_probs = temp
                        pre = c
            #print(f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)')
            #นับจำนวนสิ่งที่ทำนาย
            if(class2text[pre]=="Bacon"):
                count_Bacon+=1
            elif(class2text[pre]=="CrabStick"):
                count_CrabStick+=1
            cv2.putText(frame,f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)',(int(box[0][0]), int(box[1][1])),cv2.FONT_HERSHEY_SIMPLEX,0.8,(0,255,0),2)
        #ถ้าจำนวนอะไรมากกว่าให้โชว์ว่าในถาดนั้นเป็นวัตถุดิบอะไร
        if(count_Bacon>count_CrabStick):
            cv2.putText(frame,"Bacon",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
            if(count_Bacon<number):
                cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)
        elif(count_Bacon<count_CrabStick):
            cv2.putText(frame,"CrabStick",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
            if(count_CrabStick<number):
                cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)
        else:
            cv2.putText(frame,"?",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
        frame = cv2.imencode('.jpg', frame)[1].tobytes()
        yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        key = cv2.waitKey(1)
        if key == 27:
            break

def gen3():
    
    textfile = open('number.txt')
    number = int(textfile.readline())
    textfile.close()
    """Video streaming generator function."""
    cap = cv2.VideoCapture('Mix.mp4')

    # Read until video is completed
    while True:
        
        ref,frame = cap.read()

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv=cv2.GaussianBlur(hsv,(15,15),0)

        l_b = np.array([0, 0, 125])
        u_b = np.array([26, 255, 255]) #ตัวแรกที่เคยลอง 23 26 30 #Pre ที่ทดลองได้ l_b [0,0,0] u_b [72,255,255] ที่เคยทำปกติ l_b [0,0,125] u_b [26,255,255]

        mask = cv2.inRange(hsv, l_b, u_b)

        res = cv2.bitwise_and(frame, frame, mask=mask) #สีปกติที่ตัดฉากหลังออก

        count_Bacon = 0
        count_CrabStick = 0
        #เสริม
        contours,hierachy=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contours:
            area = cv2.contourArea(cnt)

            if area<30000 or area>120000: # Test ใช้ area<50000 or area>105000 Bacoon2 area<100000 or area>125000 Bacoon3 area<50000 or area>120000 Bacoon5 area<30000 or area>120000
                continue #ทดลองปูอัดที่ตัดแล้ว area<12500 or area>120000
            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect).astype(int)
            box = np.int0(box)
            cv2.drawContours(frame,[box],0,(0,255,0),2)
            #--------ตัดภาพ---------#
            width = int(rect[1][0])
            height = int(rect[1][1])

            src_pts = box.astype("float32")
            dst_pts = np.array([[0, height-1],
                            [0, 0],
                            [width-1, 0],
                            [width-1, height-1]], dtype="float32")
            M = cv2.getPerspectiveTransform(src_pts, dst_pts)
            warped = cv2.warpPerspective(frame, M, (width, height))
            images = []

            warped = cv2.resize(warped,(224,224))

            # Convert the Image object into a numpy array
            img = img_to_array(warped)

            # Add to a list of images
            images.append(img)

            images = np.asarray(images)

            # Preprocess the input array
            x = preprocess_input(images)

            #print(f"Image shape: {x.shape}")

            #ทำนายภาพ
            probs = model.predict(x)
            temp=0.0
            new_probs = probs[0][0]
            pre = 0
            for c in range(num_classes):
                    #print(f'{class2text[c]} ({probs[0][c]*100:.2f}%)')
                    temp=probs[0][c]
                    if(temp>new_probs):
                        new_probs = temp
                        pre = c
            #print(f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)')
            #นับจำนวนสิ่งที่ทำนาย
            if(class2text[pre]=="Bacon"):
                count_Bacon+=1
            elif(class2text[pre]=="CrabStick"):
                count_CrabStick+=1
            cv2.putText(frame,f'{class2text[pre]} ({probs[0][pre]*100:.2f}%)',(int(box[0][0]), int(box[1][1])),cv2.FONT_HERSHEY_SIMPLEX,0.8,(0,255,0),2)
        #ถ้าจำนวนอะไรมากกว่าให้โชว์ว่าในถาดนั้นเป็นวัตถุดิบอะไร
        if(count_Bacon>count_CrabStick):
            cv2.putText(frame,"Bacon",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
            if(count_Bacon<number):
                cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)
        elif(count_Bacon<count_CrabStick):
            cv2.putText(frame,"CrabStick",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
            if(count_CrabStick<number):
                cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)
        else:
            cv2.putText(frame,"?",(10,100),cv2.FONT_HERSHEY_SIMPLEX,4,(255,0,0),5,cv2.LINE_AA)
        frame = cv2.imencode('.jpg', frame)[1].tobytes()
        yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        key = cv2.waitKey(1)
        if key == 27:
            break

def gen4():
    
    """Video streaming generator function."""
    cap = cv2.VideoCapture('pork2.mp4')

    # Read until video is completed
    while True:
        
        ref,frame = cap.read()

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv=cv2.GaussianBlur(hsv,(15,15),0)

        l_b = np.array([0, 0, 125])
        u_b = np.array([35, 255, 255])

        mask = cv2.inRange(hsv, l_b, u_b)

        res = cv2.bitwise_and(frame, frame, mask=mask) #สีปกติที่ตัดฉากหลังออก
        count = 0
        #เสริม
        kernel=np.ones((5,5),np.uint8)
        mask=cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel,iterations=4)

        #เสริม
        contours,hierachy=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contours:
            area = cv2.contourArea(cnt)
            if area<550000: #or area>6900000: # Test ใช้ area<50000 or area>105000 Bacoon2 area<100000 or area>125000 Bacoon3 area<50000 or area>120000 Bacoon5 area<30000 or area>120000
                continue
            count+=1
        if(count<1):
            cv2.rectangle(frame,(6,5),(1910,1074),(0,0,255),20)

        frame = cv2.imencode('.jpg', frame)[1].tobytes()
        yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        key = cv2.waitKey(1)
        if key == 27:
            break


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/video_feed2')
def video_feed2():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen2(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/video_feed3')
def video_feed3():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen3(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/video_feed4')
def video_feed4():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen4(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/setting')
def setting():
    return render_template('setting.html')

@app.route('/save', methods=['POST'])
def save():
    number = request.form['save']
    if int(number)>0:
        textfile = open('number.txt',"w")
        textfile.write(str(number))
        textfile.close()

    return render_template('index.html')

@app.route('/change')
def change():
    return render_template('change.html')

@app.route('/change', methods=['POST'])
def mode():
    a = request.form['change']
    textfile2 = open('change.txt',"w")
    textfile2.write(str(a))
    textfile2.close()

    return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)

