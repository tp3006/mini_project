from django.db import models

# Create your models here.
class Jim(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Corn(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Chokchai(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Sufficiency(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Mountain(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Waterfall(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Park(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Garden(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Island(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class ChaAm(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class HuaHin(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Chaweng(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Rattanakosin(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Siam(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Temple(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class Historic(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class GreenHill(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class LivePark(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class DreamWorld(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)

class SafariWorld(models.Model):
    Comment=models.TextField()
    Feeling=models.CharField(max_length=15)