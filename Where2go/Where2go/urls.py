"""Where2go URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Trip import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.Home),
    path('QuestionOpen',views.Question),
    path('Jim',views.JimQ),
    path('Corn',views.CornQ),
    path('Chokchai',views.ChokchaiQ),
    path('Sufficiency',views.SufficiencyQ),
    path('Mountain',views.MountainQ),
    path('Waterfall',views.WaterfallQ),
    path('Park',views.ParkQ),
    path('Garden',views.GardenQ),
    path('Island',views.IslandQ),
    path('ChaAm',views.ChaAmQ),
    path('HuaHin',views.HuaHinQ),
    path('Chaweng',views.ChawengQ),
    path('Rattanakosin',views.RattanakosinQ),
    path('Siam',views.SiamQ),
    path('Temple',views.TempleQ),
    path('Historic',views.HistoricQ),
    path('GreenHill',views.GreenHillQ),
    path('LivePark',views.LiveParkQ),
    path('DreamWorld',views.DreamWorldQ),
    path('SafariWorld',views.SafariWorldQ),
    path('Comment',views.CommentPredict),
    path('TripPredict',views.TripPredict),
    path('Home',views.Home),
]
