import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pandas.core.frame import DataFrame
from sklearn.preprocessing import StandardScaler
from minisom import MiniSom

data = pd.read_csv('som_data.csv')
#print(data.head())
#print(data.shape)

dataset = data.drop(['id'],axis = 1) # นำ column id ออก
dataset = dataset.to_numpy()


standard = StandardScaler()
cleanDataSet = pd.DataFrame(standard.fit_transform(dataset))
cleanDataSet = cleanDataSet.to_numpy()
#print(dataset[0])
    

x_dimension = 30
y_dimension  = 30
som = MiniSom(x_dimension, y_dimension, 4, sigma=1.35 , learning_rate=0.1, neighborhood_function='gaussian', topology='hexagonal', activation_distance='euclidean')
som.pca_weights_init(cleanDataSet)
som.train(cleanDataSet, 30000, verbose=True)

label = np.zeros(70,dtype=tuple) #สร้าง array 70 ช่องพร้อมใส่เลข 0 ทุกช่อง (ให้ตัวข้อมูลเป็น tuple) ***ไม่ต้องทำเป็น array ก็ได้ สามารถใช้ list ได้เลย***
for i in range(70):
    label[i] = som.winner(cleanDataSet[i]) # คำสั่ง winner จะ return ข้อมูลเป็นชนิด tuple
data['Label'] = label
data.to_csv("Score.csv",index=False)

#สร้างข้อมูลใหม่เพื่อบันทึกแยกระดับสุขภาพจิตแต่ละประเภท
id = np.zeros(70,dtype=int)
id_temp = 0
new_data = np.empty((70,4),dtype=object)
for x in range(70):
    id[x] = id_temp+1
    id_temp+=1
    for y in range(4):
        if(y==0):
            if(dataset[x][y]>=19):
                new_data[x][y] = 'High'
            elif(dataset[x][y]>=13):
                new_data[x][y] = 'Middle'
            elif(dataset[x][y]>=7):
                new_data[x][y] = 'Low'
            else:
                new_data[x][y] = 'Normal'
        elif(y==1):
            if(dataset[x][y]>=15):
                new_data[x][y] = 'High'
            elif(dataset[x][y]>=10):
                new_data[x][y] = 'Middle'
            else:
                new_data[x][y] = 'Low'
        elif(y==2):
            if(dataset[x][y]>=62):
                new_data[x][y] = 'Severe'
            elif(dataset[x][y]>=42):
                new_data[x][y] = 'High'
            elif(dataset[x][y]>=24):
                new_data[x][y] = 'Middle'
            else:
                new_data[x][y] = 'Low'
        elif(y==3):
            if(dataset[x][y]>=19):
                new_data[x][y] = 'Severe'
            elif(dataset[x][y]>=14):
                new_data[x][y] = 'High'
            elif(dataset[x][y]>=9):
                new_data[x][y] = 'Middle'
            else:
                new_data[x][y] = 'Low'

new_data_label = pd.DataFrame(id,columns=['id'])
new_data_label['PHQ-9'] = new_data[:,0]
new_data_label['GAD-7'] = new_data[:,1]
new_data_label['SPST-20'] = new_data[:,2]
new_data_label['PISCES-10'] = new_data[:,3]
new_data_label['Label'] = label
new_data_label.to_csv("RiskLevel.csv",index=False)

#สร้างข้อมูลเพื่อ plot heatmap
label_to_count=[]
matrix_count=[[] for i in range(x_dimension)] #เก็บจำนวนสมาชิกในแต่ละช่องของ map
id_group = [[] for i in range(x_dimension)]
id_group_text=''
column = [] #นำไปสร้างเป็นชื่อ column
for x in range(70):
    label_to_count.append(label[x])
for x in range(x_dimension):
    column.append(x)
    for y in range(y_dimension):
        matrix_count[x].append(label_to_count.count((x,y)))
        for z in range(70):
            if(label[z]==tuple((x,y))):
                id_group_text+='['+str(z+1)+']'
        id_group[x].append(id_group_text)
        id_group_text=''
#print(id_group)
data_heatmap=pd.DataFrame(matrix_count,columns=column)
#print(data_heatmap)
cmap = sns.diverging_palette(h_neg=110, h_pos=130, as_cmap=True)
sns.heatmap(data_heatmap,
            annot=id_group, fmt='',
            center=0,
            linewidths=.5,
            cmap=cmap)
plt.show()