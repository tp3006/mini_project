# Mini Project
## Data Mining: Board Game Recommendation Program
Predict user behavior that is suitable for game board type.

## Artificial Intelligence: Traveling Salesperson Problem
Find the shortest path using genetic algorithm.

## Software Engineering: Smart Pantry
Limit the picking of items in pantry of sharing.

## Big Data Analysis: Where2go
Predict user behavior that is suitable for tourist attraction and sentiment analysis from comments.

## Computer Vision: Thurmlana
Detect, predict and count ingredients in a buffet restaurant.
Notification to staffs when ingredients are almost running out.

## Neural Network and Deep Learning: My Mind
Clustering people at risk of all 4 symptoms of mental health (Depression, GAD, ASD, PTSD) with Self Organizing Map (SOM).
