package com.example.smartpantry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class History extends AppCompatActivity {

    Database db;
    ImageView b1,b2,b3,b4;
    ListView todoListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        db = new Database(this);
        Bundle bundle = getIntent().getExtras();
        String username = bundle.getString("username");
        todoListView = (ListView)findViewById(R.id.Historylist);
        Database data = new Database(getApplicationContext());
        ArrayList<String> list = data.getHistory(username);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
        todoListView.setAdapter(adapter);
        b1 = (ImageView)findViewById(R.id.Donate2);
        b2 = (ImageView)findViewById(R.id.Receive2);
        b3 = (ImageView)findViewById(R.id.History2);
        b4 = (ImageView)findViewById(R.id.Pantry2);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(History.this,Donate.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(History.this,Receive.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(History.this,Pantry.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
    }
}