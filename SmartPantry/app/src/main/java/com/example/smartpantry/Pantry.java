package com.example.smartpantry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class Pantry extends AppCompatActivity {

    Database db;
    ImageView b1,b2,b3,b4;
    ListView todoListView;
    Button b5 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantry);
        db = new Database(this);
        Database data = new Database(getApplicationContext());
        ArrayList<ItemInPantry> list = new ArrayList<ItemInPantry>();
        list.add(new ItemInPantry("มาม่ารสหมูสับ",db.getItemNum("มาม่ารสหมูสับ"),R.drawable.mamamusub));
        list.add(new ItemInPantry("มาม่ารสต้มยำกุ้ง",db.getItemNum("มาม่ารสต้มยำกุ้ง"),R.drawable.mamathomyamkung));
        list.add(new ItemInPantry("มาม่ารสเย็นตาโฟต้มยำหม้อไฟ",db.getItemNum("มาม่ารสเย็นตาโฟต้มยำหม้อไฟ"),R.drawable.mamayentafoo));
        list.add(new ItemInPantry("ปลากระป๋องโรซ่า",db.getItemNum("ปลากระป๋องโรซ่า"),R.drawable.roza));

        ItemInPantryAdapter adapter = new ItemInPantryAdapter(this,list);
        ListView todoListView = (ListView)findViewById(R.id.Itemlist);
        todoListView.setAdapter(adapter);
        Bundle bundle = getIntent().getExtras();
        String username = bundle.getString("username");
        b1 = (ImageView)findViewById(R.id.Donate);
        b2 = (ImageView)findViewById(R.id.Receive);
        b3 = (ImageView)findViewById(R.id.History);
        b4 = (ImageView)findViewById(R.id.Pantry);
        b5 = (Button)findViewById(R.id.Logout);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Pantry.this,Donate.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Pantry.this,Receive.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Pantry.this,History.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Pantry.this , MainActivity.class);
                startActivity(i);
            }
        });

    }
}