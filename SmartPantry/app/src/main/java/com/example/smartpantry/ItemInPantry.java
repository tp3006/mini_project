package com.example.smartpantry;

public class ItemInPantry {
    private String mName;
    private String mNum;
    private int mImage;
    public ItemInPantry(String name,String num,int img){
        mName = name;
        mNum = num;
        mImage = img;
    }

    public String getmName(){
        return mName;
    }

    public String getmNum(){
        return mNum;
    }

    public int mImge(){
        return mImage;
    }
}
