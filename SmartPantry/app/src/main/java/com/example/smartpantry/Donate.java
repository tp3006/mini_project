package com.example.smartpantry;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Donate extends AppCompatActivity{

    Database db ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);
        db = new Database(this);
        scanCode();
    }
    private void scanCode(){
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CaptureAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning Code");
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        Bundle bundle = getIntent().getExtras();
        String username = bundle.getString("username");
        String itemname="",status = "Donate";
        if(result != null) {
            if (result.getContents() != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                String Barcode = result.getContents();
                if(Barcode.equals("8850987101014")) {
                    builder.setMessage("มาม่ารสหมูสับ");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสหมูสับ";
                }
                else if(Barcode.equals("8850987101021")){
                    builder.setMessage("มาม่ารสต้มยำกุ้ง");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสต้มยำกุ้ง";
                }
                else if(Barcode.equals("8850987142611")){
                    builder.setMessage("มาม่ารสเย็นตาโฟต้มยำหม้อไฟ");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสเย็นตาโฟต้มยำหม้อไฟ";
                }
                else if(Barcode.equals("8850511121181")){
                    builder.setMessage("ปลากระป๋องโรซ่า");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "ปลากระป๋องโรซ่า";
                }
                else if(Barcode.equals("8850987101199")){
                    builder.setMessage("มาม่ารสหมูสับแพค10");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสหมูสับแพค10";
                }
                else if(Barcode.equals("8850987101274")){
                    builder.setMessage("มาม่ารสหมูสับลัง");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสหมูสับลัง";
                }
                else if(Barcode.equals("8850987101205")){
                    builder.setMessage("มาม่ารสต้มยำกุ้งแพค10");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสต้มยำกุ้งแพค10";
                }
                else if(Barcode.equals("8850987101295")){
                    builder.setMessage("มาม่ารสต้มยำกุ้งลัง");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสต้มยำกุ้งลัง";
                }
                else if(Barcode.equals("8850987142628")){
                    builder.setMessage("มาม่ารสเย็นตาโฟต้มยำหม้อไฟแพค10");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสเย็นตาโฟต้มยำหม้อไฟแพค10";
                }
                else if(Barcode.equals("8850987142635")){
                    builder.setMessage("มาม่ารสเย็นตาโฟต้มยำหม้อไฟลัง");
                    builder.setTitle("สิ่งของบริจาค");
                    itemname = "มาม่ารสเย็นตาโฟต้มยำหม้อไฟลัง";
                }
                if(Barcode.equals("8850987101014")||Barcode.equals("8850987101021")||Barcode.equals("8850987142611")||Barcode.equals("8850511121181")||Barcode.equals("8850987101199")||Barcode.equals("8850987101274")||Barcode.equals("8850987101205")||Barcode.equals("8850987101295")||Barcode.equals("8850987142628")||Barcode.equals("8850987142635")) {
                    String finalItemname = itemname;
                    builder.setPositiveButton("ยืนยัน", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Date currentTime = Calendar.getInstance().getTime();
                            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                            String date = df.format(currentTime);
                            Boolean chkitem = db.Add(Barcode,username, finalItemname,date,status);
                            if(chkitem == true){
                                Toast.makeText(getApplicationContext() , "การบริจาคสำเร็จ" , Toast.LENGTH_SHORT).show();
                                Intent x = new Intent(Donate.this, Pantry.class);
                                startActivity(x);
                            }
                            else{
                                Toast.makeText(getApplicationContext() , "การบริจาคล้มเหลว" , Toast.LENGTH_SHORT).show();
                                Intent x = new Intent(Donate.this, Pantry.class);
                                startActivity(x);
                            }

                        }
                    }).setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent x = new Intent(Donate.this, Pantry.class);
                            startActivity(x);
                        }
                    });
                }
                else{
                    builder.setMessage("ไม่มีรหัสสิ่งของชิ้นนี้");
                    builder.setTitle("สิ่งของบริจาค");
                    builder.setPositiveButton("ยืนยัน", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent x = new Intent(Donate.this, Pantry.class);
                            startActivity(x);
                        }
                    });
                }
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else{
                Toast.makeText(this,"No Results",Toast.LENGTH_LONG).show();
            }
        }
        else {
            super.onActivityResult(requestCode,resultCode,data);
        }
    }
}
