package com.example.smartpantry;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class ItemInPantryAdapter extends ArrayAdapter<ItemInPantry> {

    private static final String LOG_TAG = ItemInPantryAdapter.class.getSimpleName();

    public ItemInPantryAdapter(Activity context, ArrayList<ItemInPantry> itemInPantries) {
        super(context,0,itemInPantries);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.layout, parent, false);
        }
        ItemInPantry currentItemInPantry = getItem(position);

        TextView nameTextView = (TextView) listItemView.findViewById(R.id.item_name);
        nameTextView.setText(currentItemInPantry.getmName());

        TextView numberTextView = (TextView) listItemView.findViewById(R.id.item_number);
        numberTextView.setText(String.valueOf(currentItemInPantry.getmNum()));

        ImageView iconView = (ImageView) listItemView.findViewById(R.id.list_item_icon);
        iconView.setImageResource(currentItemInPantry.mImge());
        return listItemView;
    }

}
