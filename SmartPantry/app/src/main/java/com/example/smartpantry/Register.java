package com.example.smartpantry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Register extends AppCompatActivity {

    Database db ;
    EditText e1,e2,e3,e4 ;
    Button b1;
    ImageView b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        db = new Database(this);
        e1 = (EditText)findViewById(R.id.usernameL);
        e2 = (EditText)findViewById(R.id.pass1);
        e3 = (EditText)findViewById(R.id.pass2);
        e4 = (EditText)findViewById(R.id.email);
        b1 = (Button)findViewById(R.id.register);
        b2 = (ImageView)findViewById(R.id.imageViewBack);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this,User.class);
                startActivity(i);
            }
        });
        b1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String s1 = e1.getText().toString();
                String s2 = e2.getText().toString();
                String s3 = e3.getText().toString();
                String s4 = e4.getText().toString();

                if (s1.equals("") || s2.equals("") || s3.equals("") || s4.equals("") ){
                    Toast.makeText(getApplicationContext() , "Wrong!" ,Toast.LENGTH_SHORT).show();
                }
                else {
                    if (s2.equals(s3)){
                        Boolean chkusername = db.chkusername(s1);
                        if (chkusername == true){
                            Boolean chkemail = db.chkemail(s4);
                            if (chkemail == true){
                                Boolean insert = db.insert(s1,s2,s4);
                                if (insert == true){
                                    Toast.makeText(getApplicationContext() , "Registered" , Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(Register.this,Login.class);
                                    startActivity(i);
                                }
                            }
                            else{
                                Toast.makeText(getApplicationContext() , "Email Already exists !!!" , Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Toast.makeText(getApplicationContext() , "Username Already exists !!!" , Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext() , "Password not match !!!" , Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }
}