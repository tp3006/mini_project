package com.example.smartpantry;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Receive extends AppCompatActivity {

    Database db;
    ImageView b1,b2,b3,b4;
    ListView todoListView;
    Button b5 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive);
        db = new Database(this);
        Database data = new Database(getApplicationContext());
        ArrayList<ItemInPantry> list = new ArrayList<ItemInPantry>();
        list.add(new ItemInPantry("มาม่ารสหมูสับ",db.getItemNum("มาม่ารสหมูสับ"),R.drawable.mamamusub));
        list.add(new ItemInPantry("มาม่ารสต้มยำกุ้ง",db.getItemNum("มาม่ารสต้มยำกุ้ง"),R.drawable.mamathomyamkung));
        list.add(new ItemInPantry("มาม่ารสเย็นตาโฟต้มยำหม้อไฟ",db.getItemNum("มาม่ารสเย็นตาโฟต้มยำหม้อไฟ"),R.drawable.mamayentafoo));
        list.add(new ItemInPantry("ปลากระป๋องโรซ่า",db.getItemNum("ปลากระป๋องโรซ่า"),R.drawable.roza));

        ItemInPantryAdapter adapter = new ItemInPantryAdapter(this,list);
        ListView todoListView = (ListView)findViewById(R.id.Itemlist);
        todoListView.setAdapter(adapter);
        todoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = getIntent().getExtras();
                String username = bundle.getString("username");
                switch (i){
                    case 0:
                        Intent x = new Intent(Receive.this, ReceiveConfirm.class);
                        String Barcode = "8850987101014";
                        String name = "มาม่ารสหมูสับ";
                        x.putExtra("Barcode",Barcode);
                        x.putExtra("username",username);
                        x.putExtra("name",name);
                        startActivity(x);
                        break;
                    case 1:
                        Intent y = new Intent(Receive.this, ReceiveConfirm.class);
                        String Barcode2 = "8850987101021";
                        String name2 = "มาม่ารสต้มยำกุ้ง";
                        y.putExtra("Barcode",Barcode2);
                        y.putExtra("username",username);
                        y.putExtra("name",name2);
                        startActivity(y);
                        break;
                    case 2:
                        Intent z = new Intent(Receive.this, ReceiveConfirm.class);
                        String Barcode3 = "8850987142611";
                        String name3 = "มาม่ารสเย็นตาโฟต้มยำหม้อไฟ";
                        z.putExtra("Barcode",Barcode3);
                        z.putExtra("username",username);
                        z.putExtra("name",name3);
                        startActivity(z);
                        break;
                    case 3:
                        Intent a = new Intent(Receive.this, ReceiveConfirm.class);
                        String Barcode4 = "8850511121181";
                        String name4 = "ปลากระป๋องโรซ่า";
                        a.putExtra("Barcode",Barcode4);
                        a.putExtra("username",username);
                        a.putExtra("name",name4);
                        startActivity(a);
                        break;
                }
            }
        });
        Bundle bundle = getIntent().getExtras();
        String username = bundle.getString("username");
        b1 = (ImageView)findViewById(R.id.Donate);
        b2 = (ImageView)findViewById(R.id.Receive);
        b3 = (ImageView)findViewById(R.id.History);
        b4 = (ImageView)findViewById(R.id.Pantry);
        b5 = (Button)findViewById(R.id.Logout);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Receive.this,Donate.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Receive.this,History.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Receive.this,Pantry.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Receive.this , MainActivity.class);
                startActivity(i);
            }
        });

    }
}