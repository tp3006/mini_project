package com.example.smartpantry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Database db;
    Button b1;
    ListView todoListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new Database(this);
        Database data = new Database(getApplicationContext());
        ArrayList<ItemInPantry> list = new ArrayList<ItemInPantry>();
        list.add(new ItemInPantry("มาม่ารสหมูสับ",db.getItemNum("มาม่ารสหมูสับ"),R.drawable.mamamusub));
        list.add(new ItemInPantry("มาม่ารสต้มยำกุ้ง",db.getItemNum("มาม่ารสต้มยำกุ้ง"),R.drawable.mamathomyamkung));
        list.add(new ItemInPantry("มาม่ารสเย็นตาโฟต้มยำหม้อไฟ",db.getItemNum("มาม่ารสเย็นตาโฟต้มยำหม้อไฟ"),R.drawable.mamayentafoo));
        list.add(new ItemInPantry("ปลากระป๋องโรซ่า",db.getItemNum("ปลากระป๋องโรซ่า"),R.drawable.roza));

        ItemInPantryAdapter adapter = new ItemInPantryAdapter(this,list);
        ListView todoListView = (ListView)findViewById(R.id.Itemlist);
        todoListView.setAdapter(adapter);
        b1 = (Button) findViewById(R.id.Login);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, User.class);
                startActivity(i);
            }
        });
    }
}