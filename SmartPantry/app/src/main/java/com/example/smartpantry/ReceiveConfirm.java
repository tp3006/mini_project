package com.example.smartpantry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ReceiveConfirm extends AppCompatActivity {

    Database db;
    TextView title,item;
    Button b1,b2,b3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_confirm);
        db = new Database(this);
        Bundle bundle = getIntent().getExtras();
        String Barcode = bundle.getString("Barcode");
        String username = bundle.getString("username");
        String name = bundle.getString("name");
        String status = "Receive";
        title = findViewById(R.id.title);
        item = findViewById(R.id.item);
        title.setText("สิ่งของที่ท่านจะได้รับ");
        item.setText(name);
        b1 = (Button)findViewById(R.id.confirm);
        b2 = (Button)findViewById(R.id.refuse);
        b3 = (Button)findViewById(R.id.Logout);
        b1.setText("ยืนยัน");
        b2.setText("ยกเลิก");
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date currentTime = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                String date = df.format(currentTime);
                Boolean chkitem = db.Decrease(Barcode,username,name,date,status);
                if(chkitem == true){
                    Toast.makeText(getApplicationContext() , "การรับของสำเร็จ" , Toast.LENGTH_SHORT).show();
                    Intent x = new Intent(ReceiveConfirm.this, Pantry.class);
                    startActivity(x);
                }
                else{
                    Toast.makeText(getApplicationContext() , "คุณได้รับของครบตามจำนวนของวันนี้แล้ว" , Toast.LENGTH_SHORT).show();
                    Intent x = new Intent(ReceiveConfirm.this, Pantry.class);
                    startActivity(x);
                }
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(ReceiveConfirm.this, Receive.class);
                startActivity(x);
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ReceiveConfirm.this , MainActivity.class);
                startActivity(i);
            }
        });
    }
}