package com.example.smartpantry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText e1,e2 ;
    Button b1;
    ImageView b2;
    Database db ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = new Database(this);
        e1 = (EditText)findViewById(R.id.usernameL);
        e2 = (EditText)findViewById(R.id.passwordL);
        b1 = (Button)findViewById(R.id.btnlogin);
        b2 = (ImageView)findViewById(R.id.imageViewBack);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = e1.getText().toString();
                String password = e2.getText().toString();
                Boolean chkuserpass = db.usernamepassword(username,password);
                if (chkuserpass == true){
                    Toast.makeText(getApplicationContext(),"Successfully!" , Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Login.this, Pantry.class);
                    i.putExtra("username",username);
                    startActivity(i);
                } else
                    Toast.makeText(getApplicationContext() ,"Wrong username or password !!!" , Toast.LENGTH_SHORT).show();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this,User.class);
                startActivity(i);
            }
        });

    }
}