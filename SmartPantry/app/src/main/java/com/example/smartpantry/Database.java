package com.example.smartpantry;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.ContentView;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Database extends SQLiteOpenHelper {
    public Database(Context context) {
        super(context,"smartpantry.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table user(username text primary key,password text,email text)");
        db.execSQL("Create table items(id integer primary key autoincrement,name text ,num integer)");
        db.execSQL("Create table history(id integer primary key autoincrement,username text ,item text,date text,status text)");
        db.execSQL("INSERT INTO items (name, num) VALUES ('มาม่ารสหมูสับ', 0)");
        db.execSQL("INSERT INTO items (name, num) VALUES ('มาม่ารสต้มยำกุ้ง', 0)");
        db.execSQL("INSERT INTO items (name, num) VALUES ('มาม่ารสเย็นตาโฟต้มยำหม้อไฟ', 0)");
        db.execSQL("INSERT INTO items (name, num) VALUES ('ปลากระป๋องโรซ่า', 0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
        db.execSQL("drop table if exists items");
        db.execSQL("drop table if exists history");
    }

    //insert

    public boolean insert(String username , String password , String email){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username",username);
        contentValues.put("password",password);
        contentValues.put("email",email);
        long ins = db.insert("user",null , contentValues );
        if (ins == 1) return false;
        else return true;
    }
    public Boolean chkusername(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user WHERE username = ?" , new String[]{username});
        if (cursor.getCount() > 0 ) return false;
        else return true;
    }

    public Boolean chkemail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user WHERE email = ?" , new String[]{email});
        if (cursor.getCount() > 0 ) return false;
        else return true;
    }
    //login
    public Boolean usernamepassword(String username , String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user WHERE username = ? and password = ?" , new String[]{username,password});
        if (cursor.getCount() > 0) return true;
        else return false ;
    }

    public Boolean Add(String Barcode,String username,String finalItemname,String date,String status){
        SQLiteDatabase db = this.getWritableDatabase();
        if(Barcode.equals("8850987101014")){
            db.execSQL("UPDATE items SET num = num+1 WHERE id=1");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987101021")){
            db.execSQL("UPDATE items SET num = num+1 WHERE id=2");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987142611")){
            db.execSQL("UPDATE items SET num = num+1 WHERE id=3");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850511121181")){
            db.execSQL("UPDATE items SET num = num+1 WHERE id=4");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987101199")){
            db.execSQL("UPDATE items SET num = num+10 WHERE id=1");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987101274")){
            db.execSQL("UPDATE items SET num = num+30 WHERE id=1");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987101205")){
            db.execSQL("UPDATE items SET num = num+10 WHERE id=2");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987101295")){
            db.execSQL("UPDATE items SET num = num+30 WHERE id=2");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987142628")){
            db.execSQL("UPDATE items SET num = num+10 WHERE id=3");
            History(username, finalItemname,date,status);
            return true;
        }
        else if(Barcode.equals("8850987142635")){
            db.execSQL("UPDATE items SET num = num+30 WHERE id=3");
            History(username, finalItemname,date,status);
            return true;
        }
        return false;
    }

    public Boolean Decrease(String Barcode,String username,String finalItemname,String date,String status){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db2 = this.getReadableDatabase();
        Cursor cursor = db2.rawQuery("SELECT  * FROM history WHERE username = ? and date = ? and status = ?" , new String[]{username,date,status});
        int count = cursor.getCount();
        Log.d("Count", String.valueOf(count));
        if(count<5) {
            if (Barcode.equals("8850987101014")) {
                db.execSQL("UPDATE items SET num = num-1 WHERE id=1");
                History(username, finalItemname,date,status);
                return true;
            } else if (Barcode.equals("8850987101021")) {
                db.execSQL("UPDATE items SET num = num-1 WHERE id=2");
                History(username, finalItemname,date,status);
                return true;
            }
            else if (Barcode.equals("8850987142611")) {
                db.execSQL("UPDATE items SET num = num-1 WHERE id=3");
                History(username, finalItemname,date,status);
                return true;
            }else if (Barcode.equals("8850511121181")) {
                db.execSQL("UPDATE items SET num = num-1 WHERE id=4");
                History(username, finalItemname,date,status);
                return true;
            }
        }
        return false;
    }

    public void History(String username,String finalItemname,String date,String status){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username",username);
        contentValues.put("item",finalItemname);
        contentValues.put("date",date);
        contentValues.put("status",status);
        long ins = db.insert("history",null , contentValues );
        if(ins==1)Log.d("Check","True");
        else Log.d("Check","False");
    }


    public ArrayList<String> getHistory(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> todoList = new ArrayList<String>();
        Cursor cursor = db.rawQuery("SELECT * FROM history WHERE username = ? ORDER BY id DESC ;" , new String[]{username});
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            todoList.add(cursor.getString(2)+"  "+cursor.getString(3)+" "+cursor.getString(4));
            cursor.moveToNext();
        }
        cursor.close();
        return todoList;
    }

    public String getItemNum(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        String num = null;
        Cursor cursor = db.rawQuery("SELECT * FROM items WHERE name = ?;",new String[]{name});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            num = cursor.getString(2);
            cursor.moveToNext();
        }
        cursor.close();
        return num;
    }
}

