from django.http import request
from django.shortcuts import render


import numpy as np, random, operator, pandas as pd, matplotlib.pyplot as plt

class City:
    def __init__(self, x, y, name): #สร้างเมืองทั้ง 25 เมือง พร้อมพิกัต x y ที่สุ่ม 0-200 นับทศนิยมด้วย
        self.x = x
        self.y = y
        self.name = name
    def getX(self):return self.x
    def getY(self):return self.y
    def getName(self):return self.name
    def distance(self, city):
        xDis = abs(self.x - city.x) #แอพสรูตระยะห่าง 2 เมือง
        yDis = abs(self.y - city.y)
        distance = np.sqrt((xDis ** 2) + (yDis ** 2)) #หาระยะทางทั้ง 2 แกน
        return distance
    
    def __repr__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"
#------------------------------------------------------------
class Fitness:
    def __init__(self, route):
        self.route = route
        self.distance = 0
        self.fitness= 0.0
    
    def routeDistance(self):
        if self.distance ==0:
            pathDistance = 0
            for i in range(0, len(self.route)):
                fromCity = self.route[i]
                toCity = None
                if i + 1 < len(self.route):
                    toCity = self.route[i + 1]
                else:
                    toCity = self.route[0]
                pathDistance += fromCity.distance(toCity) #รวมระยะทาง 2 แกนทั้งหมด
            self.distance = pathDistance
        return self.distance
    
    def routeFitness(self):
        if self.fitness == 0:
            self.fitness = 1 / float(self.routeDistance()) #ยิ่งระยะทางมากค่า fitness ยิ่งน้อย
        return self.fitness
#-----------------------------------------------
def createRoute(cityList):
    route = random.sample(cityList, len(cityList)) #สุ่มการเดินทางเป็นลำดับของแต่ละเมือง เช่น มี 1 2 3 4 5 สุ่มจะได้ลำดับ 2 5 1 3 4
    return route
#----------------------------------------------
def initialPopulation(popSize, cityList):
    population = []

    for i in range(0, popSize):
        population.append(createRoute(cityList))
    return population #ได้ list เป็น population มีทั้งหมด 100 ลำดับเส้นทาง
#------------------------------------------------
def rankRoutes(population):
    fitnessResults = {}
    for i in range(0,len(population)):
        fitnessResults[i] = Fitness(population[i]).routeFitness()
    return sorted(fitnessResults.items(), key = operator.itemgetter(1), reverse = True) #นำ 100 ลำดับเส้นทางที่เรียงค่า fitness จากมากไปน้อยกลับไป
#----------------------------------------------
def selection(popRanked, eliteSize):
    selectionResults = []
    df = pd.DataFrame(np.array(popRanked), columns=["Index","Fitness"]) #นำข้อมูลมาทำเป็นตาราง(DataFrame)
    df['cum_sum'] = df.Fitness.cumsum() #ผลรวมสะสม (อยู่ในตารางลอง print ก็ได้)
    df['cum_perc'] = 100*df.cum_sum/df.Fitness.sum() #ผลรวมสะสมแบบเปอร์เซ็น (อยู่ในตารางลอง print ก็ได้)
    
    for i in range(0, eliteSize):
        selectionResults.append(popRanked[i][0])
    for i in range(0, len(popRanked) - eliteSize):
        pick = 100*random.random()
        for i in range(0, len(popRanked)):
            if pick <= df.iat[i,3]: #นำ colum cum_perc มาเทียบค่าความน่าจะเป็น
                selectionResults.append(popRanked[i][0])
                break
    return selectionResults
#--------------------------------------------------
def matingPool(population, selectionResults): #คัดตัวที่ถูกเลือกออกมาจากประชากร
    matingpool = []
    for i in range(0, len(selectionResults)):
        index = selectionResults[i]
        matingpool.append(population[index])
    return matingpool
#-------------------------------------------------
def breed(parent1, parent2): #ผสมพันธุ์
    child = []
    childP1 = []
    childP2 = []
    
    geneA = int(random.random() * len(parent1))
    geneB = int(random.random() * len(parent1))
    
    startGene = min(geneA, geneB)
    endGene = max(geneA, geneB)

    for i in range(startGene, endGene):
        childP1.append(parent1[i]) #นำลำดับเมืองบางตัวออกมา
        
    childP2 = [item for item in parent2 if item not in childP1] #นำลำดับเมืองที่ไม่ได้อยู่ใน childP1 ออกมา

    child = childP1 + childP2 #เชื่อมกัน *****ดูตัวอย่างภาพได้ในเว็บ*****
    return child
#-----------------------------------------------------
def breedPopulation(matingpool, eliteSize):
    children = []
    length = len(matingpool) - eliteSize
    pool = random.sample(matingpool, len(matingpool)) #สุ่มออกมาเป็นลำดับดูตัวอย่างบรรทัดที่ 43

    for i in range(0,eliteSize):
        children.append(matingpool[i]) #คัดออกมา 20 ตัว
    
    for i in range(0, length):
        child = breed(pool[i], pool[len(matingpool)-i-1])
        children.append(child)
    return children
#-----------------------------------------------------
def mutate(individual, mutationRate):
    for swapped in range(len(individual)):
        if(random.random() < mutationRate):
            swapWith = int(random.random() * len(individual))
            
            city1 = individual[swapped]
            city2 = individual[swapWith]
            
            individual[swapped] = city2
            individual[swapWith] = city1 #สลับลำดับเมืองกัน 2 ตัว
    return individual
#-----------------------------------------------------
def mutatePopulation(population, mutationRate):
    mutatedPop = []
    
    for ind in range(0, len(population)):
        mutatedInd = mutate(population[ind], mutationRate)
        mutatedPop.append(mutatedInd)
    return mutatedPop
#------------------------------------------------------
def nextGeneration(currentGen, eliteSize, mutationRate):
    popRanked = rankRoutes(currentGen) # 100 ตัวที่เรียงลำดับตาม fitness จากมากไปน้อย
    selectionResults = selection(popRanked, eliteSize) #ตัวที่ถูกเลือกมา 20 ตัวแรกที่มีค่า fitness มากๆ นอกนั้นสุ่มมา
    matingpool = matingPool(currentGen, selectionResults) #ตัวที่ถูกเลือกที่คัดออกมาจากประชากร
    children = breedPopulation(matingpool, eliteSize) #รุ่นลูก
    nextGeneration = mutatePopulation(children, mutationRate)# กลายพันธุ์
    return nextGeneration
#-----------------------------------------------------------
def geneticAlgorithm(population, popSize, eliteSize, mutationRate, generations):
    pop = initialPopulation(popSize, population) #list ของประชากร หรือ ลำดับเส้นทาง 100 เส้นทาง
    print("Initial distance: " + str(1 / rankRoutes(pop)[0][1]))
    dStart.append(1 / rankRoutes(pop)[0][1])
    popRankedtest1 = rankRoutes(pop)
    for obj in pop[popRankedtest1[0][0]]: #popRankedtest คือ ตัวบอกเส้นทางลำดับที่เท่าไรจาก 100 เส้น pop คือ เส้นทางทั้งหมดของ 100 เส้น
        xstart.append(obj.getX()) #แยกคู่ลำดับจากเมืองแรกที่สั่นสุดครั้งแรก
        ystart.append(obj.getY())
        NameStart.append(obj.getName())
        CityStart.append(obj)
    print("StartCity:",pop[popRankedtest1[0][0]])
    for i in range(0, generations):
        pop = nextGeneration(pop, eliteSize, mutationRate)
    
    popRankedtest2 = rankRoutes(pop)
    for obj in pop[popRankedtest2[0][0]]:
        xstop.append(obj.getX()) #แยกคู่ลำดับจากเมืองแรกที่สั่นสุดครั้งสุดท้าย
        ystop.append(obj.getY())
        NameStop.append(obj.getName())
        CityStop.append(obj)
    print("Final distance: " + str(1 / rankRoutes(pop)[0][1]))
    dStop.append(1 / rankRoutes(pop)[0][1])
    print("StopCity:",pop[popRankedtest2[0][0]])
    bestRouteIndex = rankRoutes(pop)[0][0]
    bestRoute = pop[bestRouteIndex]
    return bestRoute
#--------------------------------------------------------------

def start(request):
    return render(request,'input.html',{'xpop':xpop,'ypop':ypop,'name':CityName,'cityList':cityList})

cityList = []
xpop = []
ypop = []
CityName =[]
xstart = []
ystart = []
CityStart = []
NameStart = []
xstop = []
ystop = []
CityStop = []
NameStop = []
dStart =[]
dStop = []

def show(request):
    xstart.clear()
    ystart.clear()
    xstop.clear()
    ystop.clear()
    dStart.clear()
    dStop.clear()
    CityStart.clear()
    CityStop.clear()
    NameStart.clear()
    NameStop.clear()
    pop = request.POST['pop']
    elite = request.POST['elite']
    mutation = request.POST['mutation']
    generation = request.POST['generation']

    print("cityList:",cityList)
#-------------------------------------population ปกติ คือ 25 popSize = 100 eliteSize = 20  mutationRate=0.01 generations=500
    geneticAlgorithm(population=cityList, popSize=int(pop), eliteSize=int(elite), mutationRate=float(mutation), generations=int(generation))
    return render(request,'index.html',{'xpop':xpop,'ypop':ypop,'xstart':xstart,'ystart':ystart,'xstop':xstop,'ystop':ystop,'dStart':dStart,'dStop':dStop,'name':CityName,'cityList':cityList,'CityStart':CityStart,'CityStop':CityStop,'NameStart':NameStart,'NameStop':NameStop})

def AddCity(request):
    xpop.clear()
    ypop.clear()
    CityName.clear()
    xCity = request.POST['x']
    yCity = request.POST['y']
    nameCity = request.POST['name']
    cityList.append(City(x=int(xCity),y=int(yCity),name=nameCity))
    for obj in cityList: #แยกคู่ลำดับพิกัดแต่ละเมือง
        xpop.append(obj.getX()) #เป็น list แบบ object
        ypop.append(obj.getY())
        CityName.append(obj.getName())
    return render(request,'input.html',{'xpop':xpop,'ypop':ypop,'name':CityName,'cityList':cityList})

def Delete(request):
    xpop.clear()
    ypop.clear()
    CityName.clear()
    cityList.pop(len(cityList)-1)
    for obj in cityList: #แยกคู่ลำดับพิกัดแต่ละเมือง
        xpop.append(obj.getX()) #เป็น list แบบ object
        ypop.append(obj.getY())
        CityName.append(obj.getName())
    return render(request,'input.html',{'xpop':xpop,'ypop':ypop,'name':CityName,'cityList':cityList})

def DeleteAll(request):
    xpop.clear()
    ypop.clear()
    CityName.clear()
    cityList.clear()
    return render(request,'input.html',{'xpop':xpop,'ypop':ypop})

def Search(request):
    xstart.clear()
    ystart.clear()
    xstop.clear()
    ystop.clear()
    dStart.clear()
    dStop.clear()
    CityStart.clear()
    CityStop.clear()
    NameStart.clear()
    NameStop.clear()
    return render(request,'index.html',{'xpop':xpop,'ypop':ypop,'xstart':xstart,'ystart':ystart,'xstop':xstop,'ystop':ystop,'dStart':dStart,'dStop':dStop,'name':CityName,'cityList':cityList,'CityStart':CityStart,'CityStop':CityStop,'NameStart':NameStart,'NameStop':NameStop})
