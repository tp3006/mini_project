from django.apps import AppConfig


class CharttestConfig(AppConfig):
    name = 'charttest'
