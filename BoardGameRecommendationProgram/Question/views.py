from django.http import request
from django.shortcuts import render
from sklearn.neighbors import KNeighborsClassifier
import pandas as pd
import numpy

def Start(request):
    return render(request,'index.html')
def Question(request):
    return render(request,'question.html')
def Show(request):

    gender = request.POST['gender']
    age = request.POST['age']
    feeling = request.POST['feeling']
    work = request.POST['work']
    match = request.POST['match']
    pretend = request.POST['pretend']
    indulgent = request.POST['indulgent']
    trust = request.POST['trust']
    help = request.POST['help']
    imagine = request.POST['imagine']
    plan = request.POST['plan']
    talk = request.POST['talk']
    food = request.POST['food']
    num = request.POST['num']
    time = request.POST['time']
    method = request.POST['method']
    complexity = request.POST['complexity']
    horoscope = request.POST['horoscope']
    name = request.POST['name']
    Test= numpy.zeros((1,27))
#เงื่อนไข gender
    if(gender=="male"):
        Test[0][0]=1
    elif(gender=="female"):
        Test[0][1]=1
#เงื่อนไข age
    if(age=="13"):
        Test[0][2]=-1.5
    elif(age=="19"):
        Test[0][2]=-0.4
    elif(age=="25"):
        Test[0][2]=0.7
    else:
        Test[0][2]=1.9
#เงื่อนไข feeling
    if(feeling=="happy"):
        Test[0][3]=1
    elif(feeling=="soso"):
        Test[0][4]=1
#เงื่อนไข work
    if(work=="team"):
        Test[0][5]=1
#เงื่อนไข match
    if(match=="1"):
        Test[0][6]=-2.5
    elif(match=="2"):
        Test[0][6]=-1.5
    elif(match=="3"):
        Test[0][6]=-0.5
    elif(match=="4"):
        Test[0][6]=0.5
    else:
        Test[0][6]=1.5
#เงื่อนไข pretend
    if(pretend=="1"):
        Test[0][7]=-2
    elif(pretend=="2"):
        Test[0][7]=-1.2
    elif(pretend=="3"):
        Test[0][7]=-0.4
    elif(pretend=="4"):
        Test[0][7]=0.4
    else:
        Test[0][7]=1.3
#เงื่อนไข indulgent
    if(indulgent=="2"):
        Test[0][8]=-2
    elif(indulgent=="3"):
        Test[0][8]=-0.8
    elif(indulgent=="4"):
        Test[0][8]=0.4
    else:
        Test[0][8]=1.6
#เงื่อนไข trust
    if(trust=="1"):
        Test[0][9]=-3.1
    elif(trust=="2"):
        Test[0][9]=-1.9
    elif(trust=="3"):
        Test[0][9]=-0.7
    elif(trust=="4"):
        Test[0][9]=0.5
    else:
        Test[0][9]=1.7
#เงื่อนไข help
    if(help=="3"):
        Test[0][10]=-1.4
    elif(help=="5"):
        Test[0][10]=1.4
#เงื่อนไข imagine
    if(imagine=="1"):
        Test[0][11]=-3
    elif(imagine=="2"):
        Test[0][11]=-2
    elif(imagine=="3"):
        Test[0][11]=-1
    elif(imagine=="4"):
        Test[0][11]=-0.1
    else:
        Test[0][11]=0.9
#เงื่อนไข plan
    if(plan=="1"):
        Test[0][12]=-2.9
    elif(plan=="2"):
        Test[0][12]=-1.9
    elif(plan=="3"):
        Test[0][12]=-0.8
    elif(plan=="4"):
        Test[0][12]=0.3
    else:
        Test[0][12]=1.3
#เงื่อนไข talk
    if(plan=="1"):
        Test[0][13]=-2.4
    elif(plan=="2"):
        Test[0][13]=-1.4
    elif(plan=="3"):
        Test[0][13]=-0.5
    elif(plan=="4"):
        Test[0][13]=0.5
    else:
        Test[0][13]=1.4
#เงื่อนไข food
    if(food=="snack"):
        Test[0][14]=1
    elif(food=="none"):
        Test[0][15]=1
    elif(food=="drink"):
        Test[0][16]=1
#เงื่อนไข num
    if(num=="2"):
        Test[0][17]=-1
    elif(num=="5"):
        Test[0][17]=0.7
    elif(num=="8"):
        Test[0][17]=2.4
#เงื่อนไข time
    if(time=="5"):
        Test[0][18]=-1.6
    elif(time=="15"):
        Test[0][18]=-0.9
    elif(time=="35"):
        Test[0][18]=-0.2
    elif(time=="65"):
        Test[0][18]=0.5
    elif(time=="95"):
        Test[0][18]=1.2
    else:
        Test[0][18]=1.9
#เงื่อนไข method
    if(method=="psychology"):
        Test[0][19]=1
#เงื่อนไข complexity
    if(complexity=="1"):
        Test[0][20]=-3.1
    elif(complexity=="2"):
        Test[0][20]=-1.9
    elif(complexity=="3"):
        Test[0][20]=-0.6
    elif(complexity=="4"):
        Test[0][20]=0.6
    else:
        Test[0][20]=1.9
#เงื่อนไข horoscope
    if(horoscope=="1"):
        Test[0][21]=-1.9
    elif(horoscope=="2"):
        Test[0][21]=-0.9
    elif(horoscope=="4"):
        Test[0][21]=0.9
    elif(horoscope=="5"):
        Test[0][21]=1.8
#เงื่อนไข name
    if(name=="patchwork"):
        Test[0][22]=1
    elif(name=="chinatown"):
        Test[0][23]=1
    elif(name=="spyfall"):
        Test[0][24]=1
    elif(name=="ticket"):
        Test[0][25]=1
    else:
        Test[0][26]=1

#อ่านข้อมูล
    df=pd.read_csv("data/TrainData.csv")

    x=df.drop("BoardGame",axis=1).values

    y=df['BoardGame'].values

    knn=KNeighborsClassifier(n_neighbors=2)

    knn.fit(x,y)

    pred=knn.predict(Test)

    print(pred)

    if(pred[0]=="Abstract Games"):
        return render(request,'result_Abstract.html')
    elif(pred[0]=="Family Games "):
        return render(request,'result_Family.html')
    elif(pred[0]=="Party Games "):
        return render(request,'result_Party.html')
    elif(pred[0]=="Strategy Games"):
        return render(request,'result_Strategy.html')
    else:
        return render(request,'result_Thematic.html')

def Rehome(request):
    return render(request,'index.html')